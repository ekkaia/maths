import { Polynom } from "./polynom";

/**
 * Prend en entrée les valeurs x_0, ..., x_n, les valeurs prescrites y_0, ..., y_n
 * ainsi qu'une collection de valeurs u_1, ..., u_m appartenant à [x_0, x_n] et
 * calcule les valeurs q(u_j) où q est la fonction construite à l'exercice 3.
 * 
 * @param xValues valeurs x_0, ..., x_n
 * @param yValues valeurs prescrites y_0, ... y_n
 * @param uValues collection u_1, ..., u_m
 * 
 * @returns p_i(u) pour tout u de uValues
 * 
 * Préconditions :
 *  - xValues est triée par ordre croissant
 *  - uvalues est triée par ordre croissant
 *  - les valeurs de uValues appartiennent à xValues
 *  - uValues.length <= xValues.length
 */
export function main(
	xValues: number[],
	yValues: number[],
	uValues: number[]
): number[] {

	//  Résoudre le système tridiagonal pour trouver z_1, ... z_(n-1)
	let zValues: number[] = solveTridiagonalSystem(xValues, yValues);
	zValues[0] = 0;         //  Ajoute 0 au début du tableau, car z_0 = 0
	zValues.push(0);        //  Ajoute 0 à la fin du tableau, car z_n = 0
	
	console.log("\nValeurs de z_i :")
	for(let i: number = 0; i < zValues.length; i++) {
		console.log(`z[${i}] = ${zValues[i]}`);
	}

	//  Pour tout i de 0 à n-1, calculer les coefficient de p_i
	let polynoms: Polynom[] = new Array<Polynom>();

	console.log("\nPolynômes :");
	for (let i: number = 0; i < xValues.length - 1; i++) {
		polynoms.push(getPolynoms(xValues, yValues, zValues, i, i + 1));
		console.log(polynoms[i].toString())
	}

	//  Pour tout u de uValues, calculer p_i(u), où i est l'index contenant u
	let pOfU: number[] = new Array<number>();
	let interval: number;

	for(let i = 0; i < uValues.length; i++) {
		interval = findInterval(uValues[i], xValues);
		pOfU[i] = polynoms[interval].getValue(uValues[i] - xValues[interval]);
	}

	return pOfU;
}

/**
 * Résoud le système triagonal, retourne les valeurs de z_0, 
 * z_1, z_2 et z_3. Implémentation de l'algorithme 2 donné 
 * dans l'énoncé du devoir.
 * 
 * @param xValues x_0, ..., x_n
 * @param yValues y_0, ..., y_n
 * 
 * @returns z_1, ..., z_n-1 : coefficients pour la construction des splines
 */
export function solveTridiagonalSystem(xValues: number[], yValues: number[]): number[] {
	let v: number[] = new Array<number>();
	let w: number[] = new Array<number>();
	let z: number[] = new Array<number>();

	let b: number;
	let t: number;
	let u: number = (xValues[2] - xValues[0]) / 3;

	v[0] = (((yValues[2] - yValues[1]) / (xValues[2] - xValues[1]))
		- ((yValues[1] - yValues[0]) / (xValues[1] - xValues[0]))) / u;

	for (let k: number = 0; k < xValues.length - 3; k++) {
		t = (xValues[k + 2] - xValues[k + 1]) / 6;
		w[k] = t / u;
		u = ((xValues[k + 3] - xValues[k + 1]) / 3) - (t * w[k]);
		b = ((yValues[k + 3] - yValues[k + 2]) / (xValues[k + 3] - xValues[k + 2]))
			- ((yValues[k + 2] - yValues[k + 1]) / (xValues[k + 2] - xValues[k + 1]));
		v[k + 1] = (b - (t * v[k])) / u;
	}

	z[xValues.length - 2] = v[xValues.length - 3]

	for (let k = xValues.length - 3; k > 0; k -= 1) {
		z[k] = v[k - 1] - (w[k - 1] * z[k + 1]);
	}

	return z;
}

/**
 * Calcule les valeurs des coefficients du polynôme p_i à partir
 * des valeurs y et z, puis renvoie p_i sous la forme d'un polynôme.
 * 
 * @param xValues       x
 * @param yValues       p_i(x)
 * @param zValues       z_i(x) = p_i''(x)
 * @param lowerBound    index du début du domaine de p_i
 * @param upperBound    index de la fin du domaine de p_i
 * 
 * @returns polynôme sous la forme a_0 + a_1*x + a_2*x² + a_3*x³
 */
export function getPolynoms(
	xValues: number[],
	yValues: number[],
	zValues: number[],
	lowerBound: number,
	upperBound: number
): Polynom {
    /**
     * On utilise les résultats de l'exercice 2.b qui
     * expriment les coefficients d'un polynôme en
     * fonction de p(a), p(b), p''(a) et p''(b), sachant
     * que z_i(x) = p_i''(x).
     */
	const a: number = xValues[lowerBound];
	const b: number = xValues[upperBound];
	const p_a: number = yValues[lowerBound];
	const p_b: number = yValues[upperBound];
	const p2_a: number = zValues[lowerBound];
	const p2_b: number = zValues[upperBound];

	const u_0: number = p_a;
	const u_1: number = ((p_b - p_a) / (b - a)) - ((((2 * p2_a) + p2_b) / 6) * (b - a));
	const u_2: number = p2_a / 2;
	const u_3: number = (p2_b - p2_a) / (6 * (b - a));

	return new Polynom([u_0, u_1, u_2, u_3]);
}

/**
 * Renvoie l'index de l'intervalle [x_i, x_i+1] contenant le point
 * u donné. Si le point est en dehors de la rangée de la liste,
 * renvoie une erreur.
 * 
 * @param u point à rechercher parmi les points de xList
 * @param xList liste ordonnée des points donnés
 * 
 * @returns index de u dans xList
 */
export function findInterval(u: number, xList: number[]): number {
	if (xList.length === 0 || u < xList[0] || u > xList[xList.length - 1]) {
		//  ne devrait normalement pas arriver avec la plateforme officielle
		throw new Error(`Provided value ${u} is out of xList bounds.`);
	} else {
		let index: number = 0;  //  initialisation à 0 pour éviter toute erreur

		for (let i = 0; i < xList.length - 1; i++) {
			if (u >= xList[i] && u <= xList[i + 1]) {
				index = i;
			}
		}

		return index;
	}
}