export class Polynom {
	private coeff: Array<number> = new Array;
	
	constructor(coeff: Array<number>) {
		// Copy the array
		this.coeff = coeff.slice();
	}

	//	Getters
	getCoeff(): Array<number> {
		return this.coeff;
	}

    // Prends x en entrée pour renvoyer f(x)
	getValue(x: number): number {
		let out: number = 0;
		
		for (let i: number = 0 ; i < this.coeff.length ; i++) {
			out += this.coeff[i] * Math.pow(x, i);
		}

		return out;
	}

    // Renvois une chaine de caractère correspondant au polynome
	toString(): string {
		let output: string = "";
		for (let i: number = 0 ; i < this.coeff.length ; i++) {
			if (i == 0) {
				output = this.coeff[i] + output;
			}
			else if (i == 1) {
				output = this.coeff[i] + "x + " + output;
			}
			else {
				output = this.coeff[i] + "x^" + i + " + " + output;
			}
		}

		return output;
	}

    // Renvoie un nouveau polynôme qui contient la dérivée de celui ci
	derivate(): Polynom {
		let output: Array<number> = this.coeff.slice().map((value: number, index: number) => value *= index);
		
		output.splice(0, 1);
		return new Polynom(output);
	}

    // Additionne les deux polynômes entre eux
	add(p: Polynom): void {
		for(let i: number = 0; i < Math.min(this.coeff.length, p.getCoeff().length); i++) {
			this.coeff[i] += p.getCoeff()[i];
		}
	}

    // Soustrait les deux polynômes entre eux
	sub(p: Polynom): void {
		for(let i: number = 0; i < Math.min(this.coeff.length, p.getCoeff().length); i++) {
			this.coeff[i] -= p.getCoeff()[i];
		}
	}

    // Multiplie les deux polynômes entre eux
	mult(p: Polynom): void {
		for(let i: number = 0; i < Math.min(this.coeff.length, p.getCoeff().length); i++) {
			this.coeff[i] *= p.getCoeff()[i];
		}
	}

    // Multiplie le polynôme par un nombre n
	multN(n: number): void {
		for(let i: number = 0; i < this.coeff.length; i++) {
			this.coeff[i] *= n;
		}
	}

    // Divise les deux polynômes entre eux
	div(p: Polynom): void {
		for(let i: number = 0; i < Math.min(this.coeff.length, p.getCoeff().length); i++) {
			this.coeff[i] /= p.getCoeff()[i];
		}
	}

    // Divise le polynôme par un nombre n
	divN(n: number): void {
		for(let i: number = 0; i < this.coeff.length; i++) {
			this.coeff[i] /= n;
		}
	}

    // Renvoie un polynôme identique
	copy(): Polynom {
		return new Polynom(this.coeff);
	}

    // Compare les coefficients du polynôme avec d'autre coefficients
	equals(n: number[]): boolean {
		
		if(this.coeff.length == n.length) {
			for(let i: number = 0; i < this.coeff.length; i++) {
				if(this.coeff[i] != n[i]) {
					return false;
				}
			}
			return true;
		}
		else {
			return false;
		}
	}
}