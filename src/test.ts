import { main, solveTridiagonalSystem, findInterval, getPolynoms } from "./main";
import { Polynom } from "./polynom";

describe("Test : Polynom.getValue", () => {
	test("getValue", () => {
		const pol: Polynom = new Polynom([2,3,4,5]);

		expect(pol.getValue(5)).toEqual(742);
	});
	test("getValue", () => {
		const pol: Polynom = new Polynom([-2,-3,-4,-5]);

		expect(pol.getValue(-5)).toEqual(538);
	});
});

describe("Test : Polynom.derivate", () => {
	test("Derivate", () => {
		const pol: Polynom = new Polynom([2,3,4,5]);

		expect(pol.derivate().equals([3,8,15])).toEqual(true);
	});
});

describe("Test : Polynom.copy", () => {
	test("Copy", () => {
		const pol: Polynom = new Polynom([2,3,4,5]);

		expect(pol.copy().equals([2,3,4,5])).toEqual(true);
	});

});

describe("Test : findInterval", () => {	
	test("Interval", () => {
		const u: number = 5;
		const xList: number[] = [1, 2, 3, 4, 12, 13, 21, 25, 28, 30, 34, 201];

		expect(findInterval(u, xList)).toBe(3);
	});
	test("Interval", () => {
		const u: number = 0;
		const xList: number[] = [0, 1, 2, 3, 4, 12, 13, 21, 25, 28, 30, 34];

		expect(findInterval(u, xList)).toBe(0);
	});
	test("Interval", () => {
		const u: number = -7;
		const xList: number[] = [-10, -8, -6, -5, -3, 0, 3, 5, 6, 8, 10];

		expect(findInterval(u, xList)).toBe(1);
	});
	test("Interval", () => {
		const u: number = 10;
		const xList: number[] = [-10, -8, -6, -5, -3, 0, 3, 5, 6, 8, 10];

		expect(findInterval(u, xList)).toBe(9);
	});
});

describe("Test : solveTriangularSystem", () => {
	test("Triangular System", () => {
		const xValues: number[] = [-7.8, -6.6, -5.4, -4.2, -3, -1.7, -1.1, 0.5, 1.5, 3.1];
		const yValues: number[] = [0.27, -0.67, 1.52, 0.05, 0.18, 0.54, -0.51, 1.71, 0.38, 0.4];

		expect(solveTridiagonalSystem(xValues, yValues)).toEqual([undefined, 4.689485117604314, -5.716273803750584, 2.9256100973980144, 0.6805000808251898, -4.539764661127004, 7.008195242785671, -5.804500169737971, 2.665288494180379]);
	});
});

describe("Test : getPolynoms", () => {
	test("getPolynoms", () => {
		const xValues: number[] = [-7.8, -6.6, -5.4, -4.2, -3, -1.7, -1.1, 0.5, 1.5, 3.1];
		const yValues: number[] = [0.27, -0.67, 1.52, 0.05, 0.18, 0.54, -0.51, 1.71, 0.38, 0.4];
		const zValues: number[] = [0, 4.689485117604314, -5.716273803750584, 2.9256100973980144, 0.6805000808251898, -4.539764661127004, 7.008195242785671, -5.804500169737971, 2.665288494180379, 0];
		expect(getPolynoms(xValues, yValues, zValues, 1, 2)).toEqual(new Polynom([-0.67, 1.0924607137083924, 2.344742558802157, -1.4452442946326256]));
	});
});