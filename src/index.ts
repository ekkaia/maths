import { main } from "./main"

console.log("X22M080 Mathématiques pour l'informatique");
console.log("Travail pratique valant examen");
console.log(`\nCodé par :
    \n - Julien Brochard
    \n - Kylian Loussouarn
    \n - Léo Olivier
    \n - Félix Saget`);
console.log("\nDu groupe 485L. \n");

//  Récupérer les valeurs sur :
//  irem.sciences.univ-nantes.fr/enseignementdist/2020/X22M080/task/
const VALEURS_X: number[] = [
    -3.1,    
    -2,    
    -0.7,    
    0.3,    
    1.7,    
    2.8,    
    4.1,    
    5.4,    
    6.9,   
    7.9
];

const VALEURS_Y: number[] = [
    -0.13,
    -0.03,
    -1.32,
    0.75,
    -0.04,
    -0.15,
    0.01,
    -1.41,
    1.16,
    0.25,
];

const VALEURS_U: number[] = [
    -1.87,
    -1.26,
    1.05,
    3.25,
    4.55,
    5.59
];

const polynomsOfU: number[] = main(VALEURS_X, VALEURS_Y, VALEURS_U);

console.log("\nA partir des données recueillies, les résultats sont les suivants :")

for(let i: number = 0; i < VALEURS_U.length; i++) {
    console.log(`p_i(${VALEURS_U[i]}) = ${polynomsOfU[i]}`);
}