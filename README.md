# REQUIREMENT
* A computer connected to the Internet
* An Internet browser
* An IDE software like Visual Studio Code
* NodeJS and npm installed
* A terminal to run the commands

# STEP TO USE:
1. CLONE THE REPO `git clone https://gitlab.com/ekkaia/maths`
2. TYPE `npm i`
3. RUN `npm run start:dev`

# MADE BY
* Julien Brochard
* Kylian Loussouarn
* Léo Olivier
* Félix Saget